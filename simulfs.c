#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct rootdir_entry{
	char name[20];
	unsigned short int size;
	unsigned short int index;	
};

struct rootdir{
	unsigned short int free_blocks;
	unsigned short int trailing;
	struct rootdir_entry list_entry[170];
	unsigned char reserved[12];
};

int filesize(FILE* fp){ //calculates filesize as bytes
	int curr = ftell(fp);
	int sz;
	fseek(fp, 0L, SEEK_END);
	sz = ftell(fp);
	fseek(fp, 0L, SEEK_SET);
	return sz;
}

struct rootdir read_rd(){
	FILE *f;
	struct rootdir rd;
	
	f = fopen("simul.fs", "rb+");
	fread(&rd.free_blocks, sizeof(unsigned short int), 1, f);
	fread(&rd.trailing, sizeof(unsigned short int), 1, f);
	fread(&rd.list_entry, sizeof(struct rootdir_entry), 170, f);
	fread(&rd.reserved, sizeof(unsigned char), 12, f);
	fclose(f);
	return rd;
}

void update_rd(struct rootdir *rd){
	FILE *f;
	
	f = fopen("simul.fs", "rb+");
	fwrite(&rd->free_blocks, sizeof(unsigned short int), 1, f);
	fwrite(&rd->trailing, sizeof(unsigned short int), 1, f);
	fwrite(&rd->list_entry, sizeof(struct rootdir_entry), 170, f);
	fwrite(&rd->reserved, sizeof(unsigned char), 12, f);
	fclose(f);
}

int find_free(struct rootdir *rd){
	int i;
	for(i=0;i<170;i++){
		if(rd->list_entry[i].index == 0) return i;
	}
	return -1;
}

int find_end(struct rootdir *rd){
	int i;
	for(i=0;i<170;i++){
		if(rd->list_entry[i].index == 0 &&
			rd->list_entry[i].size == 0 &&
			rd->list_entry[i].name[0] == 0) return i;
	}
	return -1;
}

int find_file(struct rootdir *rd, unsigned char filename[]){
	int i;
	for(i=0;i<170;i++){
		if((strcmp(rd->list_entry[i].name, filename) == 0) && rd->list_entry[i].index != 0) return i;
	}
	return -1;
}

void clear_root(struct rootdir *rd){
	int i, k;
		
	for(k=0;k<170;k++){	
		for(i=0;i<20;i++) rd->list_entry[k].name[i] = 0;
		rd->list_entry[k].size = 0;
		rd->list_entry[k].index = 0;
	}
}

int write_index(unsigned short int idx[512], int pos){
	unsigned short int current[512];
	FILE *f;
	
	f = fopen("simul.fs", "rb+");
	fseek(f, 4096+(1024*pos), SEEK_SET);
	fread(current, sizeof(unsigned short int), 512, f);
	fseek(f, 4096+(1024*pos), SEEK_SET);
	fwrite(idx, sizeof(unsigned short int), 512, f);
	fclose(f);
	return current[0];	//Returns prior value so the next free block is known
}

int write_block(unsigned char buf[1024], int pos){
	unsigned char current[1024];
	unsigned short int link[512];
	FILE *f;
	
	f = fopen("simul.fs", "rb+");
	fseek(f, 4096+(1024*pos), SEEK_SET);
	fread(link, sizeof(unsigned short int), 512, f);
	fseek(f, 4096+(1024*pos), SEEK_SET);
	fwrite(buf, sizeof(unsigned char), 1024, f);
	fclose(f);
	return link[0];	//Returns prior value so the next free block is known
}

int read_index(unsigned short int idx[512], int pos){
	FILE *f;
	
	f = fopen("simul.fs", "rb+");
	fseek(f, 4096+(1024*pos), SEEK_SET);
	fread(idx, sizeof(unsigned short int), 512, f);
	fclose(f);
}

int read_block(unsigned char buf[1024], int pos){
	FILE *f;
	
	f = fopen("simul.fs", "rb+");
	fseek(f, 4096+(1024*pos), SEEK_SET);
	fread(buf, sizeof(unsigned char), 1024, f);
	fclose(f);
}

int find_free_end(int first){
	FILE *f;
	unsigned short int ptr[512];
	unsigned short int pos;
	
	f = fopen("simul.fs", "rb+");
	pos = first - 1;
	while(1){
		fseek(f, 4096+(1024*pos), SEEK_SET);
		fread(ptr, sizeof(unsigned short int), 512, f);
		if (ptr[0] == 65535) break;
		pos = ptr[0] - 1;
	}
	fclose(f);
	return pos;
}

int free_cnt(int first){
	FILE *f;
	unsigned short int ptr[512];
	unsigned short int pos, cnt;
	
	f = fopen("simul.fs", "rb+");
	pos = first - 1;
	while(1){
		fseek(f, 4096+(1024*pos), SEEK_SET);
		fread(ptr, sizeof(unsigned short int), 512, f);
		cnt++;
		if (ptr[0] == 65535) break;
		pos = ptr[0] - 1;
	}
	fclose(f);
	return cnt;
}

void update_free(unsigned short int first, unsigned short int idxpos, unsigned short int idx[512], int blocks){
	unsigned short int end;
	unsigned short int buf[512];
	int i, k;
	
	for(i=0;i<512;i++) buf[i] = 0;

	end = find_free_end(first);
	buf[0] = idxpos;
	write_index(buf, end);
	
	end = idxpos - 1;
	for(k=0;k<blocks;k++){
		buf[0] = idx[k];
		write_index(buf, end);
		end = idx[k] - 1;
	}
	buf[0] = 65535;
	write_index(buf, end);
}

void init(){
	FILE *f;
	int i, k;
	unsigned short int blank[512];
	
	struct rootdir rd;
	
	if(remove("simul.fs") == 0)							//Tries to remove existing file
   	printf("Deleting existing filesystem...\n");	//If file was deleted, report
	printf("Initializing file system...\n");
   //Initialize here.
   f = fopen("simul.fs","wb+");
   
   rd.free_blocks = 1;
   rd.trailing = 0;
   for(i=0;i<170;i++) clear_root(&rd);
   for(i=0;i<12;i++) rd.reserved[i] = 0; 
   
   //Blankout rootdir
   fwrite(&rd.free_blocks, sizeof(unsigned short int), 1, f);
	fwrite(&rd.trailing, sizeof(unsigned short int), 1, f);
	fwrite(&rd.list_entry, sizeof(struct rootdir_entry), 170, f);
	fwrite(&rd.reserved, sizeof(unsigned char), 12, f);
   
   //Chain blank entries
   //there should be 65536 blocks, but we have 65535 (as we are limited from 1 to 65535 to indexes, one block is "lost")
   for(i=1;i<65535;i++){ 
		for(k=1;k<512;k++) blank[k] = 0;
		blank[0] = i+1;
		fwrite(blank, sizeof(unsigned short int), 512, f);
	}
	//remember that 65535 is the max "pointer" value, so it will be used as a "terminator" of free space
   fclose(f);
   
   printf("Done!\n");
}

void ls(){
	int i;
	struct rootdir rd;
	char name[20];
	int size, index;
	
	rd = read_rd();
	printf("File list:\n");
	printf("NAME\t\t\t\tSIZE\t\tINDEX\n");
	for(i=0;i<170;i++){
		index = rd.list_entry[i].index;
		if(index > 0){
			strcpy(name, rd.list_entry[i].name);
			size = rd.list_entry[i].size;
			printf("%-20s\t\t%6d\t\t%6d\n", name, size, index);
		}
	}
	printf("\nFree blocks: %d\n", free_cnt(rd.free_blocks));
}

void create(char filename[]){
	FILE *f;
	unsigned char buf[1024];
	unsigned short int idx[512];
	int size, free, i, block;
	struct rootdir rd;
	
	printf("Reading file to file system...\n");
	//Read here
	f = fopen(filename, "rb+");
	if(f==NULL){
		printf("File not found!\n");
		return;
	}
	size = filesize(f);			//Measures File
	rd = read_rd();				//Recovers RD
	if(find_file(&rd, filename) > -1){
		printf("File with this name already exists!\n");
		return;
	}
	free = find_free(&rd);		//Finds free entry
	strcpy(rd.list_entry[free].name, filename);	//Writes name
	rd.list_entry[free].size = size;					//Writes size
	rd.list_entry[free].index = rd.free_blocks;	//Points to file index block
	//Size in blocks
	if(size%1024 == 0) size = size/1024;
	else size = (size/1024) + 1;
	//Operation with blocks (all block positions are subtracted by 1 to so position 1 is memory block 0 (which is the case)
	block = rd.free_blocks;
	block = write_index(idx, block-1);	//Just to read the next free block pointer (I swear)
	for(i=0; i<size; i++){
		fread(buf, sizeof(unsigned char), 1024, f);	//Read data
		idx[i] = block;										//Block to be written is listed on index
		block = write_block(buf, block-1);					//Write block and get next position
	}
	write_index(idx, rd.list_entry[free].index-1);	//Writes index for real
	rd.free_blocks = block;	//Updates free blocks with last unused block
	update_rd(&rd);			//Writes updated rd to disk
	fclose(f);
	printf("Done!\n");
}

void read(char filename[]){
	FILE *f;
	int pos, size, rdsize, id, i;
	struct rootdir rd;
	unsigned short int idx[512];
	unsigned char buf[1024];
	char newname[21];
	
	//Read here
	rd = read_rd();				//Recovers RD
	printf("Searching for %s ...\n", filename);
	pos = find_file(&rd, filename);
	if(pos==-1){
		printf("File not found!\n");
		return;
	}
	printf("Reading file from file system...\n");
	newname[0] = '\0';
	strcat(newname,"_");
	strcat(newname,filename);
	f = fopen(newname,"wb+");
	size = rd.list_entry[pos].size;	//Gets size
	id = rd.list_entry[pos].index;	//Gets index block
	
	read_index(idx, id-1);
	i=0;
	while(size > 0){
		read_block(buf, idx[i]-1);
		if(size > 1024) rdsize = 1024;
		else rdsize = size; 
		fwrite(buf, sizeof(unsigned char), rdsize, f);
		size = size - 1024;
		i++;
	}
	fclose(f);
	
	printf("Done!\n");
}

void delete_file(char filename[]){
	struct rootdir rd;
	int pos, blocks;
	unsigned short int size, idxpos, idx[512];
	
	//Search & Destroy here
	rd = read_rd();				//Recovers RD
	pos = find_file(&rd, filename);
	if(pos == -1){
		printf("File does not exist, that's easy!\n");
		return;
	}
	printf("Deleting file from file system...\n");
	
	size = rd.list_entry[pos].size;	//Gets size
	idxpos = rd.list_entry[pos].index;	//Gets index block
	read_index(idx, idxpos-1);				// Loads index
	
	if(size%1024 == 0) blocks = size/1024;
	else blocks = (size/1024) + 1;
	
	update_free(rd.free_blocks, idxpos, idx, blocks);
	rd.list_entry[pos].index = 0;
	update_rd(&rd);

	printf("Done!\n");
}

void shell(){
	char cmd[61];
	char op[20];
	char arg[40];
	int argc = 0;
	while(1){
		printf("> ");
		fgets(cmd, 40, stdin);
		argc = sscanf(cmd, "%s %s", op, arg);
		if(argc > 0){
			if(strcmp(op,"init")==0){ //Init command
				init();
			}
			else if(strcmp(op,"create")==0){ //Create command
				if(argc < 2) printf("Missing filename!\n");
				else create(arg);
			}
			else if(strcmp(op,"read")==0){ 	//Read command
				if(argc < 2) printf("Missing filename!\n");
				else read(arg);
			}
			else if(strcmp(op,"delete")==0){ //Delete command
				if(argc < 2) printf("Missing filename!\n");
				else delete_file(arg);
			}
			else if(strcmp(op,"ls")==0){ 		//LS command
				ls();
			}
			else if(strcmp(op,"exit")==0){ 		//Exit command
				return;
			}
			else printf("Invalid command!\n");
		}
	}
}

int main(int argc, char *argv[]){	
	
	//Check arguments
	if(argc < 2){	//No arguments
		printf("simulfs.exe - File system simulator\n\n");
		printf("\tAvailable commands:\n");
		printf("\t-init\t\t: Initializes file system, deletes existing if any.\n");
		printf("\t-create <file>\t: Reads file from real system to simulated file system.\n");
		printf("\t-read <file>\t: Reads file from simulated file system to real system.\n");
		printf("\t-delete <file>\t: Deletes existing file from simulated file system.\n");
		printf("\t-ls\t\t: Lists existing files in simulated file system.\n");
		printf("\t-shell\t\t: Starts simulfs in console mode.\n");
	}
	else{ //arguments
		if(strcmp(argv[1],"-init")==0){ //Init command
			init();
		}
		else if(strcmp(argv[1],"-create")==0){ //Create command
			if(argc < 3) printf("Missing filename!\n");
			else create(argv[2]);
		}
		else if(strcmp(argv[1],"-read")==0){ 	//Read command
			if(argc < 3) printf("Missing filename!\n");
			else read(argv[2]);
		}
		else if(strcmp(argv[1],"-delete")==0){ //Delete command
			if(argc < 3) printf("Missing filename!\n");
			else delete_file(argv[2]);
		}
		else if(strcmp(argv[1],"-ls")==0){ 		//LS command
			ls();
		}
		else if(strcmp(argv[1],"-shell")==0){ 	//Shell command
			shell();
		}
	}

	return 0;
}
