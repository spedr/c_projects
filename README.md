# README #

Collection of small C programs or utilities that I wrote over the years.

# simulfs.c

Implements a simulated file system that can read and write files using native C directives. 

# parallel_bubblesort.c

Implements a simple benchmark designed for parallel systems (such as clusters), using MPI (http://mpi-forum.org/).

# DHCP_spoofing_socket.c

Implements a man-in-the-middle attack by ARP Spoofing that fools the DHCP client. Will only work on local networks,
as it's used for academic purposes only.