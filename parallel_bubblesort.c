#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"

void bubblesort(int * vector, int n)
{
    int c=0, d, change, changed =1;

    while (c < (n-1) & changed)
    {
        changed = 0;
        for (d = 0 ; d < n - c - 1; d++)
            if (vector[d] > vector[d+1])
            {
                change      = vector[d];
                vector[d]   = vector[d+1];
                vector[d+1] = change;
                changed = 1;
            }
        c++;
    }
}

int *interleaving_2_vectors(int vector1[], int tam1, int vector2[], int tam2){
	
	int *vector_aux;
	int i_aux, i1, i2;

	vector_aux = (int *)malloc(sizeof(int) *(tam1+tam2));
	i1 = 0;
	i2 = 0;

	for( i_aux = 0; i_aux < tam1+tam2; i_aux++){
		if( i1 == tam1 ){
			vector_aux[i_aux] = vector2[i2++];
		}else if( i2 == tam2 ){
			vector_aux[i_aux] = vector1[i1++];
		}else if( vector1[i1] < vector2[i2]){
			vector_aux[i_aux] = vector1[i1++];
		}else{
			vector_aux[i_aux] = vector2[i2++];
		}

	}
	
	return vector_aux;

}

void display_array(int * vector, int size)
{
    int i = 0;
    for (i=0; i < size; i++) {    
    	printf("%d, ", vector[i]);
    }
}

main(int argc, char** argv)
{
    int my_rank;  /* Process identifier */
    int proc_n;   /* Number of processes */
    int father;
    int * vector;
    int vector_size;
    int delta;
    double t1, t2;
    MPI_Status status;

    int VECTOR_SIZE = atoi(argv[1]);

    MPI_Init(&argc , &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_n);

    t1 = MPI_Wtime();

    // Workload distribution, where delta is based on the number of leaf processes
    delta = (VECTOR_SIZE/proc_n)+1;

    // Load an array if I'm not root
    if (my_rank != 0) 
    {
      	MPI_Recv(&vector_size, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
      	vector = malloc( vector_size * sizeof(int));
        MPI_Recv(&vector[0], vector_size, MPI_INT , MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
      	father = status.MPI_SOURCE;
    }
    else
    {
		printf("\nProcess %d, vector Size: %d, delta %d\n", proc_n, VECTOR_SIZE, delta);
        // I'm root, therefore generate an array of integers in reverse order
      	vector = malloc( VECTOR_SIZE * sizeof(int));
        int i;
        for (i=0; i<VECTOR_SIZE; i++)
            vector[i] = VECTOR_SIZE - i;
        vector_size = VECTOR_SIZE;
    }
  
    // divide or conquer?
    if (vector_size <= delta){ //conquistar
        bubblesort(vector, vector_size);
	}
    else
    {   // divide
      
        int half_vector = (vector_size - delta)/2;

        int rest_half_vector = half_vector;
        if ((vector_size -delta)%2 != 0) {
            rest_half_vector++;
        } 

        int left_child = my_rank * 2 + 1;
      	MPI_Send (&half_vector, 1, MPI_INT, left_child, 0, MPI_COMM_WORLD);
        MPI_Send (&vector[delta], half_vector, MPI_INT, left_child, 0, MPI_COMM_WORLD);
      
        int right_child = my_rank * 2 + 2;
      	MPI_Send (&rest_half_vector, 1, MPI_INT, right_child, 0, MPI_COMM_WORLD);
        MPI_Send (&vector[half_vector + delta], rest_half_vector, MPI_INT, right_child, 0, MPI_COMM_WORLD); 

		// Sort delta locally
		bubblesort(vector, delta);
		
        // Receive from children processes
        MPI_Recv(&vector[delta], half_vector, MPI_INT , left_child, 0, MPI_COMM_WORLD, &status);
        MPI_Recv(&vector[half_vector + delta], rest_half_vector, MPI_INT , right_child, 0, MPI_COMM_WORLD, &status);
		
        // Interleaving with the leftmost child
        int * vectorAux = interleaving_2_vectors(vector, delta, &vector[delta], half_vector);

        // Interleaving with the rightmost child
        vector = interleaving_2_vectors(vectorAux, delta + half_vector, &vector[delta+half_vector], rest_half_vector);
    }

    // If I'm not root, pass it "upwards" on the process tree
    if (my_rank !=0){
        MPI_Send (&vector[0], vector_size, MPI_INT, father, 0, MPI_COMM_WORLD);
	}
    else{
        t2 = MPI_Wtime();
		// display_array(vector, vector_size);
		printf("[DONE] time(s): %f\n", t2-t1);
	}
    MPI_Finalize();
}
